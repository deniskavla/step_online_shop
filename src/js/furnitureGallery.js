let navLinkMenu = $(".nav-item.nav-link.active").attr("id");
let gelleriIdImg = '';

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    e.target // newly activated tab
    e.relatedTarget // previous active tab
    navLinkMenu = $(e.target).attr("id");

    if (navLinkMenu === 'nav-featured') {
        console.log(navLinkMenu);
        gelleriIdImg = 6;
    } else if (navLinkMenu === 'nav-top-seller') {
        console.log(navLinkMenu);
        gelleriIdImg = 0;
    } else if (navLinkMenu === 'nav-sale-off') {
        console.log(navLinkMenu);
        gelleriIdImg = 3;
    } else if (navLinkMenu === 'nav-top-rated') {
        console.log(navLinkMenu);
        gelleriIdImg = 4;
    }
    GalleryFurnitureSaleoff($(`.img-foot[data-id=${gelleriIdImg}]`));
});

$('.list img').click(function () {
    GalleryFurnitureSaleoff($(this));
});

function GalleryFurnitureSaleoff($el) {
    console.log('func');
    $('.selected')
        .empty()
        .append(`<img src="${$el.attr('src')}"/>
                 <div class="gallery-fontSize" data-id="${$el.data('id')}"></div>
                 <div class="gallery__tabs-content--data ml-auto border-stars">
                     <h3 class="gallery-fontSize">${$el.data('name')}</h3>
                 <div class="d-flex justify-content-center">
                     <i class="fas fa-star"></i>
                     <i class="fas fa-star"></i>
                     <i class="far fa-star"></i>
                     <i class="far fa-star"></i>
                     <i class="far fa-star mb-4"></i>
                 </div>
                 </div>
                 <div class="gallery__tabs-content--price rounded-circle text-white">
                    <h3 class="summa-position">${$el.data('sale')}</h3>
                    <span class = "price-line-through">${$el.data('price')}</span>
                 </div>
`)
}
$('.img-foot').eq(6).click();

$('.buttom-people-right').click(function (event) {
    let id = $('.selected > div').data('id');
    id++;
    if (id > 5) {
        id = 0;
    }
    GalleryFurnitureSaleoff($(`.img-foot[data-id=${id}]`));
});

$('.buttom-people-left').click(function (event) {
    let id = $('.selected > div').data('id');
    id--;
    if (id === -1) {
        id = 5;
    }
    GalleryFurnitureSaleoff($(`.img-foot[data-id=${id}]`));
});