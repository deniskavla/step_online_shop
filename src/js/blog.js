// $('#carouselBlog').on('slide.bs.carousel', function (e) {
//
//     var $e = $(e.relatedTarget);
//
//     var idx = $e.index();
//     console.log("IDX :  " + idx);
//
//     var itemsPerSlide = 9;
//     var totalItems = $('.carousel-item.blog__carousel').length;
//
//     if (idx >= totalItems-(itemsPerSlide-1)) {
//         var it = itemsPerSlide - (totalItems - idx);
//         for (var i=0; i<it; i++) {
//             // append slides to end
//             if (e.direction=="left") {
//                 $('.carousel-item.blog__carousel').eq(i).appendTo('.carousel-inner.blog__carousel');
//             }
//             else {
//                 $('.carousel-item.blog__carousel').eq(0).appendTo('.carousel-inner.blog__carousel');
//             }
//         }
//     }
// });

$('.blog__tabs-circle').on('click', function() {
    for (let i = 1; i < 4; i++) {
        $("#blog-list-xl-" + i).removeClass("blog__tabs-circle-active");
    }
    for (let i = 1; i < 5; i++) {
        $("#blog-list-md-" + i).removeClass("blog__tabs-circle-active");
    }

    let idNode = $(this)[0].id;
    if (idNode.indexOf('blog-list-xl-') != -1) {
        let id = idNode.replace('blog-list-xl-', '');
        $("#blog-list-xl-" + id).addClass("blog__tabs-circle-active");
    } else if (idNode.indexOf('blog-list-md-') != -1) {
        let id = idNode.replace('blog-list-md-', '');
        $("#blog-list-md-" + id).addClass("blog__tabs-circle-active");
    }
});