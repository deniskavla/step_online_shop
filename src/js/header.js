$(document).ready(function () {

    $('.market__btn-add').on('click', function() {
        let id = $(this).attr("id").replace('market-', '');
        localStorage.setItem('product', id);
        $('.header-section__number-of-purchases').text(localStorage.length);
    });

    $('.header-section__number-of-purchases').text(localStorage.length);

    $('.header-section__cart-section').on('click', function () {
        if ($('.header-section__number-of-purchases').text() == 0) {
            $('.header-section__filled-cart').addClass('disabled');
            $('.header-section__filled-cart-total-sum').addClass('disabled');
            $('.header-section__btn--continue').addClass('disabled');
            $('.header-section__btn--buy').addClass('disabled');
            $('.header-section__empty-cart').removeClass('disabled');
        } else {
            $('.header-section__empty-cart').addClass('disabled');
            $('.header-section__filled-cart').removeClass('disabled');
            $('.header-section__filled-cart-total-sum').removeClass('disabled');
            $('.header-section__btn--continue').removeClass('disabled');
            $('.header-section__btn--buy').removeClass('disabled');
            let productNumber = localStorage.getItem('product');
            $('#cart-img').attr('src', '/img/products/furniture/' + productNumber + '.png');
            $('.header-section__filled-cart-product-name').text($('#market-name-' + productNumber).text());
            $('#cart-product-discount-price').text($('#market-price-' + productNumber).text());
            $('.header-section__filled-cart-price--old').text($('#market-old-price-' + productNumber).text());
        }
        $('#cart-quantity-sum').text('$' + ($('#cart-product-discount-price').text().replace('$', '') * $('#cart-quantity').text()));
        $('#cart-total-sum').text($('#cart-quantity-sum').text());
    })

    $('.header-section__btn--buy').on('click', function() {
        localStorage.removeItem('product');
        $('.header-section__number-of-purchases').text(localStorage.length);
    });

    $('.header-section__filled-cart-btn--remove').on('click', function() {
        if ($('#cart-quantity').text() > 1) {
            $('#cart-quantity').text(+$('#cart-quantity').text() - 1);
        };
        $('#cart-quantity-sum').text('$' + ($('#cart-product-discount-price').text().replace('$', '') * $('#cart-quantity').text()));
        $('#cart-total-sum').text($('#cart-quantity-sum').text());
    });

    $('.header-section__filled-cart-btn--add').on('click', function() {
        $('#cart-quantity').text(+$('#cart-quantity').text() + 1);
        $('#cart-quantity-sum').text('$' + (+$('#cart-product-discount-price').text().replace('$', '') * +$('#cart-quantity').text()));
        $('#cart-total-sum').text($('#cart-quantity-sum').text());
    });
});


