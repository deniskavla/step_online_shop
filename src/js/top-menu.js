$(document).ready(function () {
    $('.fa-times').hide();
    $('.top-menu__nav-btn').click(function () {
        if ($('.top-menu_section__nav-section').hasClass("show")) {
            $('.top-menu__nav-section').removeClass("bg-dark");
            $('.top-menu__nav-btn').removeClass("text-white");
            $('.top-menu__nav-btn').addClass("text-dark");
        } else {
            $('.top-menu__nav-section').addClass("bg-dark");
            $('.top-menu__nav-btn').removeClass("text-dark");
            $('.top-menu__nav-btn').addClass("text-white");
        }
        $('.fa-times').toggle();
        $('.fa-bars').toggle();
    });

    let activeNavBar;
    $(".navbar li").click(function () {
       if (activeNavBar) {
           activeNavBar.removeClass("active-navbar");
       }
       if ($(this).children('a').attr("href") != '#home') {
           $(this).addClass("active-navbar");
       }
        activeNavBar = $(this);
        let target = $(this).children('a').attr("href"),
            scrlTop = $(target).offset().top - 85;
        $("body, html").animate({scrollTop: scrlTop}, 1000, 'swing');
    });
});


