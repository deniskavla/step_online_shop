const gulp = require('gulp'),
    sass = require('gulp-sass'),
    sync = require('browser-sync'),
    clean = require('gulp-clean'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rigger = require('gulp-rigger'),
    concat = require('gulp-concat');

// Пути для сборки
const path = {
    build: {
        img: './build/img/',
        html: './build/',
        scss: './build/styles',
        js: './build/'
    },
    src: {
        img: './src/img/**/*.*',
        html: './src/html/*.html',
        htmlAll: './src/html/**/*.html',
        scss: './src/scss/**/*.scss',
        js: './src/js/*.js',
        jsAll: './src/html/**/*.js'
    },
    watch: {},//возможно и нахер не нужен. разработка вся в src
    clean: './build',

};
// Очистка папок и файлов
gulp.task('clean', function () {
    return gulp.src(path.clean, {read: false, allowEmpty: true})
        .pipe(clean());
});

// // Оптимизация изображений
// gulp.task('img', function () {
//     gulp.src(path.src.img)
//         .pipe(imagemin({
//             progressive: true,
//             svgoPlugins: [{removeViewBox: false}],
//             use: [pngquant()],
//             interlaced: true
//         }))
//         .pipe(gulp.dest(path.build.img));
// });

gulp.task('sass', function () {
    return gulp.src(path.src.scss)
        .pipe(sass())
        .pipe(concat('styles.css'))
        .pipe(gulp.dest(path.build.scss))
        .pipe(sync.reload({stream: true}))
});

gulp.task('js', function () {
    return gulp.src(path.src.js)
        .pipe(concat('script.js'))
        .pipe(gulp.dest(path.build.js));
});

gulp.task('html', function () {
    return gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(sync.reload({stream: true}))
});

gulp.task('img', function () {
    return gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img))
        .pipe(sync.reload({stream: true}))
});

gulp.task('watch', function () {
    gulp.watch(path.src.scss, gulp.series('sass'));
    gulp.watch(path.src.htmlAll, gulp.series('html'));
    gulp.watch(path.src.img, gulp.series('img'));
    gulp.watch(path.src.js, gulp.series('js'));

});

gulp.task('sync', function () {
    sync.init({
        server: {
            baseDir: path.build.html
        }
    })
});

gulp.task('default', gulp.series('clean', 'sass', 'html', 'js', 'img', gulp.parallel('watch', 'sync')));